# Magento + Docker LAMP Stack + Ansible

## About:

This repo contains multiple sections to deploy a very simple Magento app
running in docker containers on farms of servers using Ansible. You should
have access to an instance already (preferably on DigitalOcean running Ubuntu 20.X).

You should also ensure ports 80 and 443 are open to the internet on your instance's
firewall and have a domain name pointing to the IP address of that instanace as well.

## Demo:

URL: [magento-demo.konnect.dev](https://magento-demo.konnect.dev/)
ADMIN_URL: [magento-demo.konnect.dev//admin_l5qyhy](https://magento-demo.konnect.dev/admin_l5qyhy)

ADMIN INFO:

```yaml
admin_user: admin
admin_first_name: admin
admin_last_name: admin
admin_email: admin@test.com
admin_password: admin1234*
```

### Running:

```bash
pip3 install virtualenv
virtualenv .venv
source .venv/bin/activate

pip3 install ansible
# Update the main.yml file in each vars/ subfolder with your own values
# Update main.yml found in the root dir with your own IP(s)

ansible-playbook -i ./hosts main.yml
```

Sidenote: This was tested on an instance (droplet) from DigitalOcean running
Ubuntu 20.4 LTS. Some of the tasks from the prerequistes section include creating
an user account (since a new droplet there comes only with a root access initially)
and doing some changes to the `sshd_config` file to improve security and it might not
be a good idea to run this section multiple times or as it currently is.
